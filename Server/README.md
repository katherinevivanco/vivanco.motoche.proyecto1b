# Backend
## Set up application

To download the database:
```shell
$ docker run -d -p 27017:27017 -e MONGODB_USER="usuario" -e MONGODB_DATABASE="database" -e MONGODB_PASS="password" --name  database tutum/mongodb
```

Sails dependencies:
```shell
$ cd Server/
$ npm install
```

## Start application
To run the database:
```shell
$ docker start database
```

To start sails:
```shell
$ sails lift # After this command choose option 2
```

## Check the database

If you have installed a `mongo` client:
```shell
$ mongo database -u usuario -p password
```

Otherwise, enter in your container:
```shell
$ docker exec -it database bash
# mongo database -u usuario -p password
MongoDB shell version: 2.6.11
connecting to: database
> db.heroes.findOne()
...
> 
```
