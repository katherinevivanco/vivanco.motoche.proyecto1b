//
//  ClienteViewController.swift
//  Vivanco.Proyecto1B
//
//  Created by Katherine Vivanco on 13/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class ClienteViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    // MARK: - Attributes
    @IBOutlet weak var dcHeroesTableView: UITableView!
    var heroes:[HeroMapper] = []

    // MARK: - UI Functions
    override func viewWillAppear(_ animated: Bool) {
        let heroAPIClient = HeroAPIClient()
        heroAPIClient.getArrayHeroes()
        NotificationCenter.default.addObserver(self, selector: #selector(actualizarDatos), name: NSNotification.Name("actualizarAPILocalHeroes"), object: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Methods
    func actualizarDatos(_ notification:Notification) {
        guard let heroesResponse = notification.userInfo?["dcHeroes"] as? [HeroMapper] else {
            print("error")
            return
        }

        self.heroes = heroesResponse
        self.dcHeroesTableView.reloadData()
    }

    // MARK: - UI Table Methods
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.heroes.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "dcHeroesCell") as! dcHeroesTableViewCell
        cell.hero = self.heroes[indexPath.row]
        cell.fillData()
        return cell
    }

    // MARK: - UI Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "cellHeroSegue" {
            let destination = segue.destination  as! SingleHeroViewController
            if let index = self.dcHeroesTableView.indexPathForSelectedRow {
                destination.hero = self.heroes[index.row]
            }
        }
    }

}
