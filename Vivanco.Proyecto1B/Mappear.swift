//
//  Mappear.swift
//  Vivanco.Proyecto1B
//
//  Created by Katherine Vivanco on 13/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation
import ObjectMapper

class ResultsMapper: Mappable {
    
    var results:[HeroMapper]?
    required init?(map: Map) {
    }

    required init?(json: [String: [HeroMapper]]) {
        self.results = json["results"]
    }

    func mapping(map: Map) {
        results <- map["results"]
    }
}

class HeroMapper : Mappable {
    var name:String?
    var picture:UIImage?
    var origin:String?
    var publisher:String?
    var image:String? {
        didSet {
            let heroAPIClient = HeroAPIClient()
            heroAPIClient.getImage(self.image!, completionHandler: { (imageResponse) in
                DispatchQueue.main.async {
                    self.picture = imageResponse
                }
            })
        }
    }

    required init?(map: Map) {
    }

    func mapping(map: Map) {
        name <- map["name"]
        image <- map["image.icon_url"]
        origin <- map["origin.name"]
        publisher <- map["publisher.name"]
    }
}
