//
//  AppLineaViewController.swift
//  Vivanco.Proyecto1B
//
//  Created by Katherine Vivanco on 13/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class HeroViewController: UITabBarController {
    // MARK: - UI Functions
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - UI Tabs Methods
    override func viewDidAppear(_ animated: Bool) {
        let items = self.tabBar.items
        
        items?[0].title = "Heroes!"
        items?[1].title = "Saved Heroes!"
    }
}
