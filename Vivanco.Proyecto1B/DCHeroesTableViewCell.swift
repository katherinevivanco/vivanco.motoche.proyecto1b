//
//  dcHeroesTableViewCell.swift
//  Vivanco.Proyecto1B
//
//  Created by Katherine Vivanco on 13/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class dcHeroesTableViewCell: UITableViewCell {
    // MARK: - Attributes
    @IBOutlet weak var dcHeroesImage: UIImageView!
    @IBOutlet weak var dcHeroesLabel: UILabel!
    
    var hero: HeroMapper?

    // MARK: - UI Cell Functions
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    // MARK: - Fill the data
    func fillData(){
        self.dcHeroesLabel.text = hero?.name
        
        if self.hero?.picture == nil {
            let heroAPIClient = HeroAPIClient()
            heroAPIClient.getImage((self.hero?.image)!, completionHandler: { (imageResponse) in
                DispatchQueue.main.async {
                    self.dcHeroesImage.image = imageResponse
                }
            })
        } else {
            self.dcHeroesImage.image = hero?.picture
        }
    }
}
