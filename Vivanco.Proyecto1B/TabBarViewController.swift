//
//  TabBarViewController.swift
//  Vivanco.Proyecto1B
//
//  Created by Cristhian Motoche on 14/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class TabBarViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func viewDidAppear(_ animated: Bool) {
        let items = self.tabBar.items
        
        items?[0].title = "Hello world!"
        items?[0].badgeValue = "LOL"
        
        items?[1].title = "The answer is 42!"
        items?[1].badgeValue = "xD"
    }
}
