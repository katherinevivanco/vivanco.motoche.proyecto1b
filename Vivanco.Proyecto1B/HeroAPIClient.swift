//
//  HeroAPIClient.swift
//  Vivanco.Proyecto1B
//
//  Created by Cristhian Motoche & Katherine Vivanco on 15/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import Foundation
import Alamofire
import AlamofireImage
import AlamofireObjectMapper

class HeroAPIClient {
    // MARK: - Urls
    let urlAPIServer = "https://comicvine.gamespot.com/api/characters/?api_key=181f170534e27d5fbd3e2645be285050249a2b94&format=json&limit=10"
    let urlOwnAPIServer = "http://ios-sails-hero.herokuapp.com/hero"

    // MARK: - Access remote API
    func getHeroes(){
        Alamofire.request(urlAPIServer).responseObject{ (response: DataResponse<ResultsMapper>) in
            let dccomicsResponse = response.result.value
            
            if let dcArray = dccomicsResponse?.results {
                NotificationCenter.default.post(name: NSNotification.Name("actualizarHeroes"), object: nil, userInfo: [ "dcHeroes": dcArray ])
            }
        }
    }

    func getImage(_ imageURL:String, completionHandler: @escaping(UIImage)->()){
        Alamofire.request(imageURL).responseImage { response in
            if let image = response.result.value {
                completionHandler(image)
            }
        }
    }
    
    // MARK: - Access our own API
    func saveHero(_ hero:HeroMapper) {
        Alamofire.request(urlOwnAPIServer, method: .post, parameters: hero.toJSON(), encoding: JSONEncoding.default).responseJSON { response in
            if let data = response.data {
                let json = String(data: data, encoding: String.Encoding.utf8)
                print("Hero Created: \(json!)")
            }
        }
    }

    func getArrayHeroes(){
        Alamofire.request(urlOwnAPIServer).responseArray { (response: DataResponse<[HeroMapper]>) in
            if let dcArray = response.result.value {
                NotificationCenter.default.post(name: NSNotification.Name("actualizarAPILocalHeroes"), object: nil, userInfo: [ "dcHeroes": dcArray ])
            }
        }
    }
}
