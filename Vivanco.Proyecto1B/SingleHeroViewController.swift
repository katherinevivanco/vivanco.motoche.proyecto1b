//
//  SingleHeroViewController.swift
//  Vivanco.Proyecto1B
//
//  Created by Cristhian Motoche on 14/6/17.
//  Copyright © 2017 EPN. All rights reserved.
//

import UIKit

class SingleHeroViewController: UIViewController {
    // MARK: - Attributes
    var hero:HeroMapper?
    @IBOutlet var heroImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var originLabel: UILabel!
    @IBOutlet weak var publisherLabel: UILabel!

    // MARK: - UI Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nameLabel.text = hero?.name
        self.originLabel.text = hero?.origin
        self.publisherLabel.text = hero?.publisher
        self.heroImageView.image = hero?.picture
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - UI Actions
    @IBAction func saveHeroAction(_ sender: UIButton) {
        let heroAPIClient = HeroAPIClient()
        heroAPIClient.saveHero(self.hero!)
    }
}
